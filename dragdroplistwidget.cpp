/*
    ffsend-gui-qt
    Copyright (C) 2020  Abrar

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QMimeData>
#include <QDebug>

#include "dragdroplistwidget.h"

DragDropListWidget::DragDropListWidget(QWidget *parent)
    : QListWidget(parent)
{
    setAcceptDrops(true);
    setDragEnabled(true);
    setDragDropOverwriteMode(true);
    setDropIndicatorShown(true);
    setDragDropMode(QListWidget::DropOnly);

    createDropHint();
    m_urls.clear();
}

void DragDropListWidget::removeThis()
{
    if (!currentItem()) {
        return;
    }

    if (currentItem() == dropHintItem) {
        return;
    }

    QString text = currentItem()->text();
    m_urls.removeOne(text);
    delete currentItem();
}

QStringList DragDropListWidget::urls()
{
    return m_urls;
}

void DragDropListWidget::clearUrls()
{
    m_urls.clear();
}

void DragDropListWidget::setUrls(QStringList urls)
{
    if (urls.empty()) {
        return;
    }

    m_urls.append(urls);
    appendFiles(m_urls);
}

void DragDropListWidget::createDropHint()
{
    dropHintItem = new QListWidgetItem("Drop \nFiles or folders \nhere", this);
    dropHintItem->setTextAlignment(Qt::AlignCenter);
    dropHintItem->setSizeHint(sizeHint());
}

void DragDropListWidget::dragEnterEvent(QDragEnterEvent *e)
{
    if (e->mimeData()->hasUrls()) {
        e->acceptProposedAction();
    } else {
        QListWidget::dragEnterEvent(e);
    }
}

void DragDropListWidget::dragMoveEvent(QDragMoveEvent *e)
{
    if (e->mimeData()->hasUrls()) {
        e->acceptProposedAction();
    } else {
        QListWidget::dragMoveEvent(e);
    }
}

void DragDropListWidget::dragLeaveEvent(QDragLeaveEvent *e)
{
    e->accept();
}

void DragDropListWidget::dropEvent(QDropEvent *e)
{
    if (e->mimeData()->hasUrls()) {
        QList<QUrl> urls = e->mimeData()->urls();

        if (!urls.isEmpty()) {
            for (const QUrl &url : urls) {
                m_urls.append(url.toLocalFile());
            }

            appendFiles(m_urls);
        }

        e->acceptProposedAction();
    }

    QListWidget::dropEvent(e);
}

void DragDropListWidget::appendFiles(QStringList items)
{
    if (dropHintItem) {
        delete dropHintItem;
        dropHintItem = 0;
    }

    clear();
    addItems(items);
    emit fileAdded();
}
