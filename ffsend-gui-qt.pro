QT       += widgets

TEMPLATE = app
TARGET = ffsend-gui-qt
VERSION = 1.0.0

# Disable warnings, enable threading support and c++11
CONFIG  += thread silent build_all c++11

# Build location
isEmpty( BUILD_PREFIX ) {
        BUILD_PREFIX = ./build
}

MOC_DIR       = $$BUILD_PREFIX/moc/
OBJECTS_DIR   = $$BUILD_PREFIX/obj/
RCC_DIR       = $$BUILD_PREFIX/qrc/
UI_DIR        = $$BUILD_PREFIX/uic/

unix {
        isEmpty(PREFIX) {
                PREFIX = /usr
        }

        target.path = $$PREFIX/bin
        INSTALLS += target
}

SOURCES += \
    dragdroplistwidget.cpp \
    ffsendgui.cpp \
    main.cpp

HEADERS += \
    dragdroplistwidget.h \
    ffsendgui.h

FORMS += \
    ffsendgui.ui

RESOURCES += \
    resource.qrc
