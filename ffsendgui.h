/*
    ffsend-gui-qt
    Copyright (C) 2020  Abrar

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <QWidget>
#include <QProcess>

namespace Ui {
    class ffsendGUI;
}

class ffsendGUI : public QWidget {
    Q_OBJECT

public:
    explicit ffsendGUI(QWidget *parent = nullptr);
    ~ffsendGUI();

private slots:
    void on_doRename_clicked(bool checked);
    void on_addFiles_clicked();
    void on_addFolders_clicked();
    void on_upload_clicked();
    void on_clear_clicked();
    void on_copyURL_clicked();
    void on_copyImage_clicked();
    void on_saveImage_clicked();
    void on_newSend_clicked();

private:
    Ui::ffsendGUI *ui;
    QProcess *proc;

    bool checkffsend();

    void initialize();
    void updateControls();

    void procStateChanged(QProcess::ProcessState state);
    void procFinished(int exitCode, QProcess::ExitStatus status);

    QImage getQRImage(const QByteArray &termOutput);
};
