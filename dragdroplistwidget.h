/*
    ffsend-gui-qt
    Copyright (C) 2020  Abrar

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <QWidget>
#include <QListWidget>
#include <QDragEnterEvent>
#include <QDropEvent>

class DragDropListWidget : public QListWidget {
    Q_OBJECT
public:
    DragDropListWidget(QWidget *parent);

    void removeThis();
    QStringList urls();
    void clearUrls();
    void setUrls(QStringList urls);

    void createDropHint();

signals:
    void fileAdded();

protected:
    void dragEnterEvent(QDragEnterEvent *e);
    void dragMoveEvent(QDragMoveEvent *e);
    void dragLeaveEvent(QDragLeaveEvent *e);
    void dropEvent(QDropEvent *e);

private:
    QStringList m_urls;
    QListWidgetItem *dropHintItem;

    void appendFiles(QStringList items);
};
