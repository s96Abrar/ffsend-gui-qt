# ffsend-gui-qt

A frontend GUI for the [ffsend](https://gitlab.com/timvisee/ffsend/-/tree/master) program using Qt framework. This is a plugin for [CuboCore CoreApps](https://gitlab.com/cubocore) too.

# Dependency

* [ffsend](https://gitlab.com/timvisee/ffsend/-/tree/master#install) 
* [Qt Framework](https://www.qt.io/)

# Build 

Build ffsend-gui-qt from source.

## For Arch user

```
# Install git and dependency
sudo pacman -S git qt5-base

# To Install `ffsend` follow ffsend installation instructions.

# Clone the repository
git clone https://gitlab.com/s96Abrar/ffsend-gui-qt
cd ffsend-gui-qt

# Build the source
qmake -qt5 && make

# Run ffsend gui
./ffsend-gui-qt

# To install ffsend gui
sudo make install
```

## For debian user

```
# Install git
sudo apt-get install git

# Install dependency
sudo apt-get install qtbase5-dev

# To Install `ffsend` follow ffsend installation instructions.

# Clone the repository
git clone https://gitlab.com/s96Abrar/ffsend-gui-qt
cd ffsend-gui-qt

# Build the source
qmake -qt5 && make

# Run ffsend gui
./ffsend-gui-qt

# To install ffsend gui
sudo make install
```