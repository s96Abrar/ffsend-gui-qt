/*
    ffsend-gui-qt
    Copyright (C) 2020  Abrar

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QFileDialog>
#include <QListView>
#include <QTreeView>
#include <QDebug>
#include <QPainter>
#include <QClipboard>

#include "dragdroplistwidget.h"
#include "ffsendgui.h"
#include "ui_ffsendgui.h"

ffsendGUI::ffsendGUI(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ffsendGUI)
{
    ui->setupUi(this);

    proc = new QProcess(this);

    connect(proc, &QProcess::stateChanged, this, &ffsendGUI::procStateChanged);
    connect(proc, static_cast<void(QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished),
    [ = ](int exitCode, QProcess::ExitStatus exitStatus) {
        procFinished(exitCode, exitStatus);
        resize(sizeHint());
    });

    connect(proc, &QProcess::readyReadStandardError, [&]() {
        ui->lblProgress->setText(proc->readAllStandardError());
        resize(sizeHint());
    });

    initialize();
}

ffsendGUI::~ffsendGUI()
{
    delete ui;
}

void ffsendGUI::initialize()
{
    ui->stackedWidget->setCurrentIndex(checkffsend());
    ui->controls->setVisible(0);
    ui->QRWid->setVisible(0);
    ui->clear->setVisible(0);

    resize(sizeHint());

    connect(static_cast<DragDropListWidget *>(ui->filesList),
            &DragDropListWidget::fileAdded, this, &ffsendGUI::updateControls);
}

void ffsendGUI::updateControls()
{
    ui->clear->setVisible(1);
    ui->newName->setVisible(0);
    ui->controls->setVisible(1);

    resize(sizeHint());
}

void ffsendGUI::procStateChanged(QProcess::ProcessState state)
{
    if (state == QProcess::Running) {
        ui->lblProgress->setText("Please wait while the file is uploading...");
    } else if (state == QProcess::Starting) {
        ui->stackedWidget->setCurrentIndex(2);
    }
    resize(sizeHint());
}

void ffsendGUI::procFinished(int exitCode, QProcess::ExitStatus status)
{
    Q_UNUSED(exitCode)

    if (status == QProcess::NormalExit) {
        ui->stackedWidget->setCurrentIndex(3);
        ui->uploadURL->clear();
        QByteArray output = proc->readAllStandardOutput();

        if (!ui->genQRCode->isChecked()) {
            ui->uploadURL->appendPlainText(output.trimmed());
            resize(sizeHint());
            return;
        }

        auto list = output.split('\n');
        ui->uploadURL->appendPlainText(list.at(0).trimmed());
        ui->uploadURL->appendPlainText(list.at(1).trimmed());

        output.clear();

        for (int i = 2; i < list.count(); i++) {
            output += list.at(i) + "\n";
        }

        ui->QRWid->setVisible(1);
        ui->lblQRCode->setPixmap(QPixmap::fromImage(getQRImage(output)));
        resize(sizeHint());
    } else {
        qDebug() << "ERROR!!!" << "Process finished unexpectedly. Proc status: " << status;
    }
}

QImage ffsendGUI::getQRImage(const QByteArray &termOutput)
{
    /*
        WHITE_ALL: '\u2588',
        WHITE_BLACK: '\u2580',
        BLACK_WHITE: '\u2584',
        BLACK_ALL: ' ',
    */

    /*
        U+2580  ▀ \xe2\x96\x80    append  UPPER HALF BLOCK
        U+2584  ▄ \xe2\x96\x84    append  LOWER HALF BLOCK
        U+2588  █ \xe2\x96\x88    append  FULL BLOCK
    */
    QTextStream output(termOutput);
    QString str = output.readAll();
    str = str.remove("\u001B[0m");
    str = str.replace("\u001B[48;5;15m\u001B[38;5;0m ", " ");
    str = str.replace("\u001B[48;5;0m\u001B[38;5;15m ", "█");
    str = str.replace("\u001b[48;5;0m\u001b[38;5;15m\xE2\x96\x84", "▀");
    str = str.replace("\u001b[48;5;15m\u001b[38;5;0m\xE2\x96\x84", "▄");

    int dpiX = 96;
    int dpiY = 96;

    QSizeF size = QSizeF(/*8.27 * dpiX, 11.69 * dpiY*/ 400, 400);

    QTextDocument doc;
    doc.setPlainText(str);
    doc.setPageSize(size);
    doc.setDefaultFont(QFont("monospace, 9"));

    QImage img = QImage(size.toSize(), QImage::Format_ARGB32);
    img.fill(Qt::white);
    img.setDotsPerMeterX(39.37008 * dpiX);
    img.setDotsPerMeterY(39.37008 * dpiY);

    QPainter painter(&img);
    painter.setPen(Qt::black);
    painter.drawRect(QRectF(QPointF(0, 0), size - QSizeF(1, 1)));
    painter.setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing | QPainter::HighQualityAntialiasing);
    painter.translate(QPointF(0, 0/*-size.height() * i*/));
    painter.translate(QPointF(0, 0));

    painter.save();

    // document.drawContents();
    QAbstractTextDocumentLayout::PaintContext ctx;
    ctx.palette.setColor(QPalette::Text, painter.pen().color());
//        if (rect.isValid())
//        {
//            p->setClipRect(rect);
//            ctx.clip = rect;
//        }
    doc.documentLayout()->draw(&painter, ctx);
    painter.restore();

    painter.end();

    return img;
}

void ffsendGUI::on_doRename_clicked(bool checked)
{
    ui->newName->setVisible(checked);
    ui->newName->setEnabled(checked);
    resize(sizeHint());
}

void ffsendGUI::on_addFiles_clicked()
{
    QStringList selectedList = QFileDialog::getOpenFileNames(this, "Select files to upload", QDir::homePath());
    if (!selectedList.count()) {
        qDebug() << "Warning!" << "No file selected.";
        return;
    }
    ui->filesList->setUrls(selectedList);
}

void ffsendGUI::on_addFolders_clicked()
{
    QFileDialog *folderDialog = new QFileDialog(this);
    folderDialog->setFileMode(QFileDialog::DirectoryOnly);
    folderDialog->setOption(QFileDialog::DontUseNativeDialog, true);
    QListView *fileLView = folderDialog->findChild<QListView *>();

    if (fileLView) {
        fileLView->setSelectionMode(QAbstractItemView::MultiSelection);
    }

    QTreeView *fileTView = folderDialog->findChild<QTreeView *>();

    if (fileTView) {
        fileTView->setSelectionMode(QAbstractItemView::MultiSelection);
    }

    if (folderDialog->exec()) {
        QStringList list = folderDialog->selectedFiles();
        if (!list.count()) {
            qDebug() << "Warning!" << "No folder selected.";
            return;
        }
        ui->filesList->setUrls(list);
    }
}

void ffsendGUI::on_clear_clicked()
{
    if (ui->filesList->currentItem()) {
        ui->filesList->removeThis();
//        delete ui->filesList->takeItem(ui->filesList->currentRow());
    }
}

void ffsendGUI::on_upload_clicked()
{
    QStringList args;
    args << "upload";

    args << "-v";

    if (ui->doArchive->isChecked()) {
        args << "-a";
    }

    if (ui->genQRCode->isChecked()) {
        args << "-Q";
    }

    if (ui->doRename->isChecked()) {
        if (ui->newName->text().count()) {
            args << "-n" << ui->newName->text();
        }
    }

    if (ui->expireTime->currentIndex() != 2) {
        if (ui->expireTime->currentIndex() == 1) {
            args << "-e" << "3600";
        } else {
            args << "-e" << "300";
        }
    }

    if (!ui->filesList->urls().count()) {
        return;
    }

    args << ui->filesList->urls();

    proc->setProgram("ffsend");
    proc->setArguments(args);
    proc->start();
}

void ffsendGUI::on_copyURL_clicked()
{
    QString str = ui->uploadURL->toPlainText();

    QApplication::clipboard()->setText(str.split('\n').at(0).split(": ").at(1).trimmed());
}

void ffsendGUI::on_copyImage_clicked()
{
    QApplication::clipboard()->setPixmap(*ui->lblQRCode->pixmap());
}

void ffsendGUI::on_saveImage_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this, "Select where to save the QR Code", QDir::homePath(), "*.png");
    if (!fileName.count()) {
        qDebug() << "Warning! " << "Empty file path for saving image.";
        return;
    }

    QImage img = ui->lblQRCode->pixmap()->toImage();
    img.save(fileName, "PNG", 100);
}

bool ffsendGUI::checkffsend()
{
    return QFile::exists("/usr/bin/ffsend");
}

void ffsendGUI::on_newSend_clicked()
{
    ui->filesList->clear();
    ui->filesList->clearUrls();
    ui->filesList->createDropHint();
    ui->doArchive->setChecked(1);
    ui->genQRCode->setChecked(0);
    ui->doRename->setChecked(0);
    ui->newName->clear();
    ui->lblProgress->clear();
    ui->lblQRCode->clear();
    ui->lblQRCode->setPixmap(QPixmap());
    ui->uploadURL->clear();
    ui->stackedWidget->setCurrentIndex(1);
    ui->controls->setVisible(0);
    resize(QSize(400, 350));
}
